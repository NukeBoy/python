from datetime import *
import codecs
import configparser
import json
import urllib.parse

import postgresql
import redis
import requests


def get_time_to_update():
    conf = configparser.RawConfigParser()
    file = codecs.open("config.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    uptime = conf.get("update",
                      "time")
    file.close()
    return uptime


def get_port_to_websocket():
    conf = configparser.RawConfigParser()
    file = codecs.open("config.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    port = conf.get("websocket",
                    "port")
    file.close()
    return port


def check_change_count(request):
    conf = configparser.RawConfigParser()
    file = codecs.open("config.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    count = conf.get("page",
                     "count")
    s = list(request.text.split("&"))
    if s[0].find('cnt=') != -1:
        tm_count = s[0].replace('cnt=', '')
        r_count = urllib.parse.unquote(u"" + tm_count + "")
        if r_count != count:
            conf.set("page",
                     "count",
                     r_count)
            with open("config.ini",
                      "w",
                      encoding='utf-8') as config:
                conf.write(config)
                count = conf.get("page",
                                 "count")
                file.close()
            return count
        else:
            return count
    else:
        return count


def check_change_order(request):
    conf = configparser.RawConfigParser()
    file = codecs.open("config.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    orders = conf.get("page",
                      "order")
    s = list(request.text.split("&"))
    if s[0].find('srt=') != -1:
        tm_order = s[0].replace('srt=', '')
        r_order = urllib.parse.unquote(u"" + tm_order + "")
        if r_order != orders:
            conf.set("page",
                     "order",
                     r_order)
            with open("config.ini",
                      "w",
                      encoding='utf-8') as config:
                conf.write(config)
                orders = conf.get("page",
                                  "order")
                file.close()
            return orders

        else:
            return orders
    else:
        return orders


def check_change_tag(request):
    conf = configparser.RawConfigParser()
    file = codecs.open("requests.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    tag = conf.get("page",
                   "request")
    s = list(request.text.split("&"))
    if s[0].find('rqst=') != -1:
        tm_tag = s[0].replace('rqst=', '')
        r_tag = urllib.parse.unquote(u"" + tm_tag + "")
        if tag != r_tag:
            conf.set("page",
                     "request",
                     r_tag)
            with open("requests.ini",
                      "w",
                      encoding='utf-8') as config:
                conf.write(config)
                tag = conf.get("page",
                               "request")
                file.close()
            return tag

        else:
            return tag
    else:
        return tag


def check_change_rqt(request):
    conf = configparser.RawConfigParser()
    file = codecs.open("requests.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    rqt = conf.get("page",
                   "rqt")
    s = list(request.text.split("&"))
    if s[0].find('rqst=') != -1:
        r_rqt = s[0].replace('rqst=', '')
        if rqt != r_rqt:
            conf.set("page",
                     "rqt",
                     r_rqt)
            with open("requests.ini",
                      "w",
                      encoding='utf-8') as config:
                conf.write(config)
                rqt = conf.get("page",
                               "rqt")
                file.close()
            return rqt

        else:
            return rqt
    else:
        return rqt


def get_connect_postgre():
    conf = configparser.RawConfigParser()
    file = codecs.open("config.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    user = conf.get("postgresql",
                    "login")
    port = conf.get("postgresql",
                    "port")
    adress = conf.get("postgresql",
                      "ip")
    db = conf.get("postgresql",
                  "base")
    password = conf.get("postgresql",
                        "password")
    file.close()
    try:
        conn = postgresql.open("pq://" + user +
                               ":" + password +
                               "@" + adress +
                               ":" + port +
                               "/" + db)
        return conn
    except:
        print("I am unable to connect to the PostgreSQL database")


def get_connect_redis():
    conf = configparser.RawConfigParser()
    file = codecs.open("config.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    port = conf.get("redis",
                    "port")
    adress = conf.get("redis",
                      "ip")
    db = conf.get("redis",
                  "base")
    file.close()
    try:
        r = redis.StrictRedis(host=adress,
                              port=port,
                              db=db)
        return r
    except:
        print("I am unable to connect to the Redis database")


def find_exist_postgre_base():
    cn = get_connect_postgre()
    exst = cn.query("""SELECT count(relname) AS count FROM pg_class, pg_namespace 
                       WHERE  pg_namespace.nspname='public' AND relname = 'history';""")
    if exst[0][0] == 0:
        if cn.execute("""CREATE TABLE history 
                         (id SERIAL PRIMARY KEY, 
                          schtitle CHAR(255), 
                          intitle CHAR(255),
                          url CHAR(500), 
                          title CHAR(500), 
                          creation_date CHAR(255), 
                          last_activity_date CHAR(255),
                          time_request CHAR(255));"""):
            return 1
        else:
            return 0
    else:
        return 1


def insert_info_postgre(request, tag, pages, info):
    page = 0
    cn = get_connect_postgre()
    ins = cn.prepare("""INSERT INTO history 
                        (schtitle, intitle, url, title, creation_date, last_activity_date, time_request) 
                         VALUES ($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP)""")
    sel = cn.prepare("""SELECT count(id) FROM history WHERE title = $1""")
    while page < pages:
        url = info['items'][page]["link"]
        title = urllib.parse.unquote(u"" + info['items'][page]["title"] + "")
        create_date = datetime.fromtimestamp(info['items'][page]["creation_date"]).strftime(
            '%Y-%m-%d %H:%M:%S')
        last_actvity_date = datetime.fromtimestamp(info['items'][page]["last_activity_date"]). \
            strftime('%Y-%m-%d %H:%M:%S')
        count = sel(title)
        if count[0][0] == 0:
            ins(str(request), str(tag), url, title, create_date, last_actvity_date)
        page += 1
    cn.close()


def insert_info_redis(tag, info, count, order, page):
    r = get_connect_redis()
    info_texts = json.dumps(info)
    title = tag + "" + str(count) + "" + str(order) + "" + str(page)
    r.set(title, info_texts)
    conf = configparser.RawConfigParser()
    file = codecs.open("config.ini",
                       "r",
                       "utf8")
    conf.read_file(file)
    r.expire(title, conf.get("redis",
                             "life_time"))
    file.close()
    del r


def find_info_redis(tag, count, order, page):
    r = get_connect_redis()
    title = tag + "" + str(count) + "" + str(order) + "" + str(page)
    if not r.get(title):
        return 0
    else:
        return json.loads(r.get(title))


def search_info(tag, request, pages, page=1, sort=0, order="desc"):
    redis_response = find_info_redis(tag=tag,
                                     count=pages,
                                     order=order,
                                     page=page)
    if redis_response == 0:
        sorts = ['activity',
                 'votes',
                 'creation',
                 'relevance']
        site = "http://api.stackexchange.com"
        url = "/2.2/search?page=%s&pagesize=%s&order=%s&sort=%s&tagged=%s&site=stackoverflow"\
              % (str(page), str(pages), str(order), sorts[sort], str(request))
        src_code = requests.get(site+url)
        info = src_code.json()
        insert_info_redis(tag=tag,
                          info=info,
                          count=pages,
                          order=order,
                          page=page)
        redis_response = find_info_redis(tag=tag,
                                         count=pages,
                                         order=order,
                                         page=page)
        if find_exist_postgre_base() == 1:
            insert_info_postgre(request,
                                tag,
                                int(pages),
                                info)
        return redis_response
    else:
        return redis_response


def get_count_all_question(tag):
    site = "http://api.stackexchange.com"
    url = "/2.2/tags/%s/info?order=desc&sort=activity&site=stackoverflow" % str(tag)
    number_jsn = requests.get(site+url).json()
    try:
        all_number = int(number_jsn['items'][0]["count"])
        return all_number
    except:
        err = number_jsn
        return err


def create_output(pages, info, tag):
    page = 0
    a = {}
    while page < pages:
        b = {0: tag, 1: info['items'][page]["link"],
             2: urllib.parse.unquote(u"" + info['items'][page]["title"] + ""),
             3: datetime.fromtimestamp(info['items'][page]["creation_date"]).strftime('%Y-%m-%d %H:%M:%S'),
             4: datetime.fromtimestamp(info['items'][page]["last_activity_date"]).strftime('%Y-%m-%d %H:%M:%S')}
        a[page] = b
        page += 1
    return a
