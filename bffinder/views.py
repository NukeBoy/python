from pyramid.view import view_config
from .searching_module import *


@view_config(route_name='search', renderer='templates/new_search.jinja2')
def new_search_views(request):
    count = check_change_count(request)
    order = check_change_order(request)
    tag = check_change_tag(request)
    return {'count': count,
            'order': order}


@view_config(route_name='home', renderer='templates/table_tag.jinja2')
def table_tag_views(request):
    cn = get_connect_postgre()
    count = check_change_count(request)
    order = check_change_order(request)
    port = get_port_to_websocket()
    find_exist_postgre_base()
    tags = cn.prepare("""SELECT intitle FROM history GROUP BY intitle LIMIT $1""")
    last_time = cn.prepare("""SELECT last_activity_date FROM history WHERE intitle = $1 
                              ORDER BY last_activity_date DESC LIMIT 1""")
    outputs = []
    for row in tags(int(count)):
        tag = str(row[0]).rstrip()
        for lt in last_time(tag):
            time = lt[0][:lt[0].find(".")]
        info = [tag, time]
        outputs.append(info)
    if order == "desc":
        output = sorted(outputs,
                        key=lambda tup: (tup[1]),
                        reverse=True)
    else:
        output = sorted(outputs,
                        key=lambda tup: (tup[1]))
    return {'items': output,
            'count': count,
            'order': order,
            'pnumber': 1,
            'port': port}


@view_config(route_name='homes', renderer='templates/table_tag.jinja2')
def table_tags_views(request):
    count = check_change_count(request)
    rlist = (str(request.decode('utf-8')).split("\r\n")[0]).split(" ")
    if all([(rlist[0] == "GET"),
            (rlist[1].find("/page") != -1)]):
        pnumber = int(rlist[1].replace("/page", ""))
        offset = count * pnumber if pnumber != 1 else 0
    else:
        pnumber = 1
        offset = 0
    cn = get_connect_postgre()
    order = check_change_order(request)
    port = get_port_to_websocket()
    find_exist_postgre_base()
    tags = cn.prepare("""SELECT intitle FROM history GROUP BY intitle LIMIT $1 OFFSET $2;""")
    last_time = cn.prepare("""SELECT last_activity_date FROM history WHERE intitle = $1 
                              ORDER BY last_activity_date DESC LIMIT 1;""")
    outputs = []
    for row in tags(int(count), offset):
        tag = str(row[0]).rstrip()
        for lt in last_time(tag):
            ltime = lt[0][:lt[0].find(".")]
        info = [tag, ltime]
        outputs.append(info)
    if order == "desc":
        output = sorted(outputs,
                        key=lambda tup: (tup[1]),
                        reverse=True)
    else:
        output = sorted(outputs,
                        key=lambda tup: (tup[1]))
    return {'items': output,
            'count': count,
            'order': order,
            'pnumber': pnumber,
            'port': port}


@view_config(route_name='results', renderer='templates/table_info.jinja2')
def table_info_view(request):
    rlist = (str(request.decode('utf-8')).split("\r\n")[0]).split(" ")
    if all([(rlist[0] == "GET"),
            (rlist[1].find("/results/page") != -1)]):
        pnumber = int(rlist[1].replace("/results/page", ""))
    else:
        pnumber = 1
    rqt = check_change_rqt(request)
    count = check_change_count(request)
    order = check_change_order(request)
    tag = check_change_tag(request)
    plain_json = search_info(tag=tag,
                             request=rqt,
                             pages=count,
                             page=pnumber,
                             sort=2,
                             order=order)
    output = create_output(pages=int(count),
                           info=plain_json,
                           tag=tag)
    fcount = get_count_all_question(rqt)
    return {'project': tag,
            'items': output,
            'count': count,
            'order': order,
            'pnumber': pnumber,
            'fcount': fcount}
