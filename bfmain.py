from pyramid.paster import get_app
from wsgiref.simple_server import make_server
from bffinder.searching_module import *

import threading
import configparser
import codecs
import time
import os.path as path
import asyncio
import websockets
import json


class HttpServer:
    def __call__(self, *args, **kwargs):
        scriptpath = path.dirname(__file__)
        filename = path.join(scriptpath, 'production.ini')
        application = get_app(filename, 'main')
        server = make_server('0.0.0.0', 6543, application)
        print("Server running")
        server.serve_forever()


class BackgroundHandler:
    def __call__(self, *args, **kwargs):
        uptime = get_time_to_update()
        print("Handler running")
        conf = configparser.RawConfigParser()
        file = codecs.open("config.ini", "r", "utf8")
        conf.read_file(file)
        while True:
            time.sleep(int(uptime))
            count = conf.get("page", "count")
            order = conf.get("page", "order")
            cn = get_connect_postgre()
            tags = cn.query("""SELECT intitle FROM history GROUP BY intitle""")
            schtitle = cn.prepare("""SELECT schtitle FROM history WHERE intitle = $1 LIMIT 1""")
            for row in tags:
                tag = str(row[0]).rstrip()
                for st in schtitle(tag):
                    rqt = str(st[0])
                search_info(tag, rqt, count, 1, 2, order)


class WebsocketServer:
    def __call__(self, *args, **kwargs):
        async def send_info(websocket, path):
            cn = get_connect_postgre()
            while True:
                tags = cn.query("""SELECT intitle FROM history GROUP BY intitle""")
                last_time = cn.prepare("""SELECT last_activity_date FROM history WHERE intitle = $1
                                       ORDER BY last_activity_date DESC LIMIT 1""")
                outputs = []
                for row in tags:
                    tag = str(row[0]).rstrip()
                    for lt in last_time(tag):
                        ltime = lt[0].rstrip()
                    info = [tag, ltime]
                    outputs.append(info)
                greeting = json.dumps(outputs)
                await websocket.send(greeting)
                await asyncio.sleep(60)
        conf = configparser.RawConfigParser()
        file = codecs.open("config.ini", "r", "utf8")
        conf.read_file(file)
        ip = conf.get("websocket", "ip")
        port = conf.get("websocket", "port")
        try:
            start_server = websockets.serve(send_info, ip, port)
            loop.run_until_complete(start_server)
            print('Websocket server running')
            loop.run_forever()
        except OSError:
            if port == 65535:
                port = 1
            else:
                port = int(port) + 1
            conf.set("websocket", "port", port)
            with open("config.ini", "w", encoding='utf-8') as config:
                conf.write(config)
                port = conf.get("websocket", "port")
                file.close()



if __name__ == '__main__':
    hs = HttpServer()
    bh = BackgroundHandler()
    ws = WebsocketServer()
    loop = asyncio.get_event_loop()
    ths = threading.Thread(target=hs)
    tbh = threading.Thread(target=bh)
    tws = threading.Thread(target=ws,
                           args=(loop,))
    ths.start()
    tbh.start()
    tws.start()
    ths.join()
    tbh.join()
    tws.join()
